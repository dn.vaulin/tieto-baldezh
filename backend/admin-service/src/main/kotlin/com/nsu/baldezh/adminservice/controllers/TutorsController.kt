package com.nsu.baldezh.adminservice.controllers

import com.nsu.baldezh.adminservice.entities.Course
import com.nsu.baldezh.adminservice.entities.Tutor
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.client.postForEntity
import java.net.URI

@RestController
@RequestMapping("admin/tutor")
class TutorsController(
    @Value("\${tutor_service_host}") val tutorHost: String,
) {

    @PostMapping
    fun addTutor(@RequestBody tutor: Tutor): ResponseEntity<String> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        return RestTemplate().postForEntity(
            URI("$tutorHost/tutor"),
            HttpEntity<Tutor>(tutor, headers)
        )
    }

    @PutMapping
    fun updateTutor(@RequestBody tutor: Tutor) {
        return RestTemplate().put("$tutorHost/tutor", tutor)
    }

    @GetMapping
    fun getTutors(): ResponseEntity<Array<Tutor>> {
        return RestTemplate().getForEntity(
            "$tutorHost/tutor"
        )
    }

    @DeleteMapping
    fun deleteTutor(@RequestParam("id") id: Int) {
        return RestTemplate().delete("$tutorHost/tutor?id=${id}")
    }
}