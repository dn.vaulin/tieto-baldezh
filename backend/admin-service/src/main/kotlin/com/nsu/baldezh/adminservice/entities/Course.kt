package com.nsu.baldezh.adminservice.entities

data class Course(var courseId: Int?, val courseName: String)