package com.nsu.baldezh.adminservice.entities

import com.fasterxml.jackson.annotation.JsonFormat
import java.sql.Date

data class Review(
    var reviewId: Int? = null,
    val tutor: Tutor? = null,
    val course: Course? = null,
    val nickname: String?,
    val text: String? = null,
    val score: Short? = null,
    val eduStart: Short? = null,
    val eduEnd: Short? = null,
    @JsonFormat(pattern = "yyyy-MM-dd")
    var reviewDate: Date? = null
)
