package com.nsu.baldezh.adminservice.entities

data class Tutor(
    var tutorId: Int?,
    val firstName: String,
    val lastName: String,
    val patronymic: String
)