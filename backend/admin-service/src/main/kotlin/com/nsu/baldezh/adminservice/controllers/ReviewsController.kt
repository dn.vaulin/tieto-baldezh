package com.nsu.baldezh.adminservice.controllers

import com.nsu.baldezh.adminservice.entities.Review
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import java.net.URI

@RestController
@RequestMapping("admin/reviews")
class ReviewsController(
    @Value("\${database_service_host}") val reviewHost: String,
    ) {

    @PostMapping("/accept")
    fun acceptProposedReview(@RequestBody review: Review): ResponseEntity<String> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        return RestTemplate().postForEntity(
            URI("$reviewHost/review/accept"),
            HttpEntity<Review>(review, headers),
            String::class.java
        )
    }

    @DeleteMapping
    fun deleteProposedReview(@RequestParam("id") reviewId: Int) {
        return RestTemplate().delete("$reviewHost/review?id=$reviewId")
    }

    @GetMapping
    fun getAllProposedReviews(): ResponseEntity<Array<Review>> {
        return RestTemplate().getForEntity("$reviewHost/review/proposed")
    }
}