package com.nsu.baldezh.adminservice.controllers

import com.nsu.baldezh.adminservice.entities.Course
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import org.springframework.http.HttpEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.getForEntity
import org.springframework.web.client.postForEntity
import java.net.URI

@RestController
@RequestMapping("admin/course")
class CoursesController(
    @Value("\${course_service_host}") val courseHost: String,
) {

    @PostMapping
    fun addCourse(@RequestBody course: Course): ResponseEntity<Unit> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        return RestTemplate().postForEntity(
            URI("$courseHost/course"),
            HttpEntity<Course>(course, headers)
        )
    }

    @PutMapping
    fun updateCourse(@RequestBody course: Course) {
        return RestTemplate().put("$courseHost/course", course)
    }

    @GetMapping
    fun getCourses(): ResponseEntity<Array<Course>> {
        return RestTemplate().getForEntity(
            "$courseHost/course"
        )
    }

    @DeleteMapping
    fun deleteCourse(@RequestParam("id") id: Int) {
        return RestTemplate().delete("http://localhost:8060/course?id=${id}")
    }
}