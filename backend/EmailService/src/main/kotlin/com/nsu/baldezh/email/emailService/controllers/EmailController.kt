package com.nsu.baldezh.email.emailService.controllers

import com.nsu.baldezh.email.emailService.services.EmailSenderService
import com.nsu.baldezh.email.emailService.entity.Mail
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/email")
class EmailController(private val emailSenderService: EmailSenderService) {

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun sendMail(@RequestBody mail: Mail) = emailSenderService.sendMail(mail)
}