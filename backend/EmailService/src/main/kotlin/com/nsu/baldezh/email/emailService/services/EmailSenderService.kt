package com.nsu.baldezh.email.emailService.services

import com.nsu.baldezh.email.emailService.entity.Mail
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Service
@Component
class EmailSenderService(private val javaMailSender: JavaMailSender) {

    private val serviceEmail = "baldezh.service@gmail.com"

    fun sendMail(mail: Mail) {
        val simpleMailMessage = SimpleMailMessage()

        simpleMailMessage.setFrom(serviceEmail)
        simpleMailMessage.setTo(serviceEmail)
        simpleMailMessage.setText(mail.text)

        javaMailSender.send(simpleMailMessage)
    }
}