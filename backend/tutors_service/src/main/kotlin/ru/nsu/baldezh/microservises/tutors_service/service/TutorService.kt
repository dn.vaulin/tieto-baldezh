package ru.nsu.baldezh.microservises.tutors_service.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.client.postForEntity
import ru.nsu.baldezh.microservises.tutors_service.entity.Tutor


interface TutorService {
    fun getTutors(): ResponseEntity<Array<Tutor>>
    fun addTutor(tutor: Tutor): ResponseEntity<String>
    fun updateTutor(tutor: Tutor)
    fun deleteTutor(tutorId: Int)
}

@Component
class DatabaseServiceHost(
    @Value("\${database_service_host}") val host: String
)

@Component
class TutorServiceImpl(val dbHost: DatabaseServiceHost) : TutorService {


    override fun getTutors(): ResponseEntity<Array<Tutor>> {
        return RestTemplate().getForEntity(
            dbHost.host + "/tutor"
        )
    }

    override fun addTutor(tutor: Tutor): ResponseEntity<String> {
        return RestTemplate().postForEntity(
            dbHost.host + "/tutor",
            HttpEntity(tutor)
        )
    }

    override fun updateTutor(tutor: Tutor) {
        return RestTemplate().put(
            dbHost.host + "/tutor", tutor
        )
    }

    override fun deleteTutor(tutorId: Int) {
        return RestTemplate().delete(
            dbHost.host + "/tutor?id=" + tutorId
        )
    }


}