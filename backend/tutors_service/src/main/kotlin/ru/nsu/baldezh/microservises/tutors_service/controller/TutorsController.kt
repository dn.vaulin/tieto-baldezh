package ru.nsu.baldezh.microservises.tutors_service.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.nsu.baldezh.microservises.tutors_service.entity.Tutor
import ru.nsu.baldezh.microservises.tutors_service.service.TutorService

@RestController
@RequestMapping("tutor")
class TutorController(
    val tutorService: TutorService
) {
    @GetMapping
    fun getTutors() = tutorService.getTutors()

    @PostMapping
    fun addTutor(@RequestBody tutor: Tutor): ResponseEntity<String> = tutorService.addTutor(tutor)

    @PutMapping
    fun updateTutorInfo(@RequestBody tutor: Tutor) = tutorService.updateTutor(tutor)

    @DeleteMapping
    fun deleteTutor(@RequestParam(value = "id") id: Int) = tutorService.deleteTutor(id)
}