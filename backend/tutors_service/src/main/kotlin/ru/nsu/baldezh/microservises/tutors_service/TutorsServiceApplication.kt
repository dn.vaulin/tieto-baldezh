package ru.nsu.baldezh.microservises.tutors_service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TutorsServiceApplication

fun main(args: Array<String>) {
	runApplication<TutorsServiceApplication>(*args)
}
