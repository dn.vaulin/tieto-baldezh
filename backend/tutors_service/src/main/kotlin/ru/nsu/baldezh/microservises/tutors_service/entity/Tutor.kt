package ru.nsu.baldezh.microservises.tutors_service.entity

data class Tutor(
    val tutorId: Int?,
    val firstName: String,
    val lastName: String,
    val patronymic: String
)