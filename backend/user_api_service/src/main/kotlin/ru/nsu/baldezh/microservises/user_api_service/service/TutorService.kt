package ru.nsu.baldezh.microservises.user_api_service.service

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import ru.nsu.baldezh.microservises.user_api_service.config.ServiceHosts
import ru.nsu.baldezh.microservises.user_api_service.domain.Tutor
import ru.nsu.baldezh.microservises.user_api_service.domain.TutorList

interface TutorService {
    fun getTutors(): ResponseEntity<TutorList>
}

@Component
class TutorServiceImpl(
    val hosts: ServiceHosts
) : TutorService {

    override fun getTutors(): ResponseEntity<TutorList> {
        val restTemplate = RestTemplate()
        val resourceUrl = hosts.tutorHost + "/tutor"
        val res: ResponseEntity<Array<Tutor>> = restTemplate.getForEntity(resourceUrl)
        return ResponseEntity(res.body?.let { TutorList(it.toList()) }, res.statusCode)
    }

}