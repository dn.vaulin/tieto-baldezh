package ru.nsu.baldezh.microservises.user_api_service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CreateReviewApplication

fun main(args: Array<String>) {
	runApplication<CreateReviewApplication>(*args)
}
