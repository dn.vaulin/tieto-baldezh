package ru.nsu.baldezh.microservises.user_api_service.service

import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.client.postForEntity
import ru.nsu.baldezh.microservises.user_api_service.config.ServiceHosts
import ru.nsu.baldezh.microservises.user_api_service.domain.Course
import ru.nsu.baldezh.microservises.user_api_service.domain.Review
import ru.nsu.baldezh.microservises.user_api_service.domain.ReviewList
import ru.nsu.baldezh.microservises.user_api_service.domain.Tutor
import ru.nsu.baldezh.microservises.user_api_service.controller.OrderBy
import ru.nsu.baldezh.microservises.user_api_service.controller.ReviewFilter
import java.text.SimpleDateFormat
import java.util.*

interface ReviewService {
    fun addReview(review: Review)
    fun getReviews(orderBy: OrderBy?, reviewFilter: ReviewFilter): ReviewList
}

@Component
class ReviewServiceMockImpl(
    val hosts: ServiceHosts
) : ReviewService {

    override fun addReview(review: Review) {
        RestTemplate().postForEntity<Review>(
            hosts.dbHost + "/review",
            review
        )
    }

    override fun getReviews(orderBy: OrderBy?, reviewFilter: ReviewFilter): ReviewList {
        val format = SimpleDateFormat("yyyy-MM-dd")
        val params = mutableListOf<String>()
        if (reviewFilter.tutorId != null) params.add("tutor_id=${reviewFilter.tutorId}")
        if (reviewFilter.courseId != null) params.add("course_id=${reviewFilter.courseId}")
        if (reviewFilter.score != null) params.add("score=${reviewFilter.score}")
        if (reviewFilter.dateStart != null) params.add("review_date_start=${format.format(reviewFilter.dateStart)}")
        if (reviewFilter.dateEnd != null) params.add("review_date_end=${format.format(reviewFilter.dateEnd)}")
        return ReviewList(
            RestTemplate().getForEntity<Array<Review>>(
                hosts.dbHost + "/review/filter?order_by=${orderBy!!.field}" + params.joinToString(
                    separator = "&",
                    prefix = "&"
                )
            ).body!!.toList()
        )
    }
}
