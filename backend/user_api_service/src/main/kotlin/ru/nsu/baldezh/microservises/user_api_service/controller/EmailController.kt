package ru.nsu.baldezh.microservises.user_api_service.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForEntity
import ru.nsu.baldezh.microservises.user_api_service.config.ServiceHosts
import ru.nsu.baldezh.microservises.user_api_service.service.CaptchaService


data class Feedback(val text: String, val recaptchaToken: String)

@RestController
@RequestMapping("api/feedback")
class EmailController(
    val hosts: ServiceHosts,
    val captchaService: CaptchaService,
) {
    @PostMapping
    fun sendFeedback(@RequestBody feedback: Feedback): ResponseEntity<Unit> {
        if (!captchaService.verify(feedback.recaptchaToken)) {
            return ResponseEntity(HttpStatus.TOO_MANY_REQUESTS)
        }
        RestTemplate().postForEntity<Feedback>(
            hosts.emailHost + "/email",
            feedback
        )
        return ResponseEntity.ok().build()
    }
}