package ru.nsu.baldezh.microservises.user_api_service.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.nsu.baldezh.microservises.user_api_service.service.CourseService

@RestController
@RequestMapping("api/course")
class CourseController(
    val courseService: CourseService
) {
    @GetMapping
    fun getCourses() = courseService.getSubjects()
}