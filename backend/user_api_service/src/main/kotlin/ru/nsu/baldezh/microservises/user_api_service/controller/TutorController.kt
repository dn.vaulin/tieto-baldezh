package ru.nsu.baldezh.microservises.user_api_service.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.nsu.baldezh.microservises.user_api_service.domain.TutorList
import ru.nsu.baldezh.microservises.user_api_service.service.TutorService

@RestController
@RequestMapping("api/tutor")
class TutorController(
    val tutorService: TutorService
) {
    @GetMapping
    fun getTutors() : ResponseEntity<TutorList> = tutorService.getTutors()
}