package ru.nsu.baldezh.microservises.user_api_service.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ServiceHosts(
    @Value("\${database_service_host}") val dbHost: String,
    @Value("\${tutor_service_host}") val tutorHost: String,
    @Value("\${course_service_host}") val courseHost: String,
    @Value("\${email_service_host}") val emailHost: String,
    @Value("\${front_host}") val frontHost: String,
)