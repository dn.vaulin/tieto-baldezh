package ru.nsu.baldezh.microservises.user_api_service.service

import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity

data class CaptchaResponse(
    val success: Boolean,
)

@Component
class CaptchaService {
    fun verify(token: String): Boolean {
        val url = "https://www.google.com/recaptcha/api/siteverify"
        val params = "?secret=6LdVmhkeAAAAAOerRAlsUUUTK0qf4Sj7IyD5NAKD&response=$token";
        val res = RestTemplate().getForEntity<CaptchaResponse>(url + params)
        return res.body?.success == true
    }
}