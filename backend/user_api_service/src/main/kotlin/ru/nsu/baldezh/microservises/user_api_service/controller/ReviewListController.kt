package ru.nsu.baldezh.microservises.user_api_service.controller

import org.springframework.core.convert.converter.Converter
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.nsu.baldezh.microservises.user_api_service.domain.Review
import ru.nsu.baldezh.microservises.user_api_service.domain.ReviewList
import ru.nsu.baldezh.microservises.user_api_service.service.ReviewService
import java.util.*

enum class OrderBy(val field: String, val comparator: Comparator<Review>) {
    SCORE("score", Comparator { o1, o2 -> o1.score.compareTo(o2.score) }),
    REVIEW_DATE("reviewDate", Comparator { o1, o2 -> o1.reviewDate.compareTo(o2.reviewDate) }),
    EDU_START("eduStart", Comparator { o1, o2 -> o1.eduStart.compareTo(o2.eduStart) })
}

@Component
class StringToFilterByEnumConverter : Converter<String, OrderBy> {
    override fun convert(source: String): OrderBy? =
        try {
            OrderBy.valueOf(source.uppercase())
        } catch (e: IllegalArgumentException) {
            null
        }
}

data class ReviewFilter(
    val tutorId: Int? = null,
    val courseId: Int? = null,
    val score: Short? = null,
    val dateStart: Date? = null,
    val dateEnd: Date? = null,
    val eduStart: Short? = null
)

@RestController
@RequestMapping("api/reviews")
class ReviewListController(
    val converter: StringToFilterByEnumConverter,
    val reviewService: ReviewService,
) {

    @GetMapping
    fun getReviewList(
        @RequestParam("order_by") orderBy: String?,
        @RequestParam("tutor_id") tutorId: Int?,
        @RequestParam("course_id") courseId: Int?,
        @RequestParam("score") score: Short?,
        @RequestParam("review_date_start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) dateStart: Date?,
        @RequestParam("review_date_end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) dateEnd: Date?,
        @RequestParam("edu_start") eduStart: Short?,

        ): ReviewList = reviewService.getReviews(
        orderBy?.let(converter::convert),
        ReviewFilter(tutorId, courseId, score, dateStart, dateEnd, eduStart)
    )
}