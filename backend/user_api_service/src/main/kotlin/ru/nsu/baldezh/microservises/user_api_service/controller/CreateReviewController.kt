package ru.nsu.baldezh.microservises.user_api_service.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.nsu.baldezh.microservises.user_api_service.domain.Review
import ru.nsu.baldezh.microservises.user_api_service.service.CaptchaService
import ru.nsu.baldezh.microservises.user_api_service.service.ReviewService

data class CreateReviewForm(
    val review: Review,
    val recaptchaToken: String,
)

@RestController
@RequestMapping("api/review")
class CreateReviewController(
    val reviewService: ReviewService,
    val captchaService: CaptchaService,
) {
    @PostMapping
    fun createReview(@RequestBody reviewForm: CreateReviewForm) : ResponseEntity<Unit> {
        if (!captchaService.verify(reviewForm.recaptchaToken)) {
            return ResponseEntity(HttpStatus.TOO_MANY_REQUESTS)
        }
        println(reviewForm.review.toString())
        reviewService.addReview(reviewForm.review)
        return ResponseEntity(HttpStatus.OK)
    }
}