package ru.nsu.baldezh.microservises.user_api_service.service

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import ru.nsu.baldezh.microservises.user_api_service.config.ServiceHosts
import ru.nsu.baldezh.microservises.user_api_service.domain.Course
import ru.nsu.baldezh.microservises.user_api_service.domain.CourseList

interface CourseService {
    fun getSubjects() : ResponseEntity<CourseList>
}

@Component
class CourseServiceImpl(
    val hosts : ServiceHosts
) : CourseService{
    override fun getSubjects(): ResponseEntity<CourseList> {
        val restTemplate = RestTemplate()
        val resourceUrl = hosts.courseHost + "/course"
        val res: ResponseEntity<Array<Course>> = restTemplate.getForEntity(resourceUrl)
        return ResponseEntity(res.body?.let { CourseList(it.toList()) }, res.statusCode)
    }
}