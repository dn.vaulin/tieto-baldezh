package ru.nsu.baldezh.microservises.user_api_service.domain

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
data class TutorList(
    var tutorList: List<Tutor>
)

data class ReviewList(
    val reviewList: List<Review>
)

data class CourseList(
    val courseList: List<Course>
)

data class Tutor(
    val tutorId: Int,
    val firstName: String,
    val lastName: String,
    val patronymic: String
)

data class Course(
    val courseId: Int,
    val courseName: String
)

data class Review(
    val tutor: Tutor?,
    val course: Course?,
    val nickname: String?,
    val text: String,
    val score: Short,
    val eduStart: Short,
    val eduEnd: Short,
    @JsonFormat(pattern = "yyyy-MM-dd")
    val reviewDate: Date
)