package com.nsu.baldezh.subjectsservice.data

import com.nsu.baldezh.subjectsservice.config.HostConfig
import com.nsu.baldezh.subjectsservice.entities.Subject
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import java.net.URI

@Component
class SqlSubjectStorage (val dbHostConfig: HostConfig): SubjectsStorage {
    val dbHost = dbHostConfig.dbHost//"http://localhost:1337"

    override fun getSubjects(): ResponseEntity<Array<Subject>> {
        return RestTemplate().getForEntity(
            "$dbHost/course",
        )
    }

    override fun addSubject(subject: Subject): ResponseEntity<String> {
        return RestTemplate().postForEntity(
            URI("$dbHost/course"),
            HttpEntity<Subject>(subject),
            String::class.java
        )
    }

    override fun updateSubject(subject: Subject) {
        RestTemplate().put("$dbHost/course", subject)
    }

    override fun deleteSubject(id: Int) {
        return RestTemplate().delete("$dbHost/course?id=${id}")
    }
}