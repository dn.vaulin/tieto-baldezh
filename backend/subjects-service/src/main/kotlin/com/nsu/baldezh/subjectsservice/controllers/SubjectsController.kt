package com.nsu.baldezh.subjectsservice.controllers;

import com.nsu.baldezh.subjectsservice.data.SubjectsStorage
import com.nsu.baldezh.subjectsservice.entities.Subject
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("course")
class SubjectsController(val subjectsStorage: SubjectsStorage) {

    @GetMapping
    fun getSubjects(): ResponseEntity<Array<Subject>> = subjectsStorage.getSubjects()

    @PostMapping
    fun addSubject(@RequestBody subject: Subject): ResponseEntity<Unit> {
        subjectsStorage.addSubject(subject)
        return ResponseEntity.ok().build()
    }

    @PutMapping
    fun updateSubject(@RequestBody subject: Subject): ResponseEntity<Unit> {
        subjectsStorage.updateSubject(subject)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping
    fun deleteSubject(@RequestParam("id") id: Int) {
        return subjectsStorage.deleteSubject(id)
    }

}
