package com.nsu.baldezh.subjectsservice.entities

data class Subject(
    val courseId: Int?,
    val courseName: String
)