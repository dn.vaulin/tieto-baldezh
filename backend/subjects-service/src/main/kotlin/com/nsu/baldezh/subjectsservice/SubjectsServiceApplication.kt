package com.nsu.baldezh.subjectsservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SubjectsServiceApplication

fun main(args: Array<String>) {
	runApplication<SubjectsServiceApplication>(*args)
}
