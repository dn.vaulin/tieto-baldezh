package com.nsu.baldezh.subjectsservice.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
data class HostConfig(
    @Value("\${database_service_host}") val dbHost: String
)
