package com.nsu.baldezh.subjectsservice.data;

import com.nsu.baldezh.subjectsservice.entities.Subject;
import org.springframework.http.ResponseEntity

interface SubjectsStorage {
    fun getSubjects(): ResponseEntity<Array<Subject>>
    fun addSubject(subject: Subject): ResponseEntity<String>
    fun updateSubject(subject: Subject)
    fun deleteSubject(id: Int)
}
