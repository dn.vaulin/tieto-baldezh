package ru.nsu.baldezh.service.db_service.model.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "review")
public class Review {
    @Id
    @Column(name = "review_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reviewId;

    @ManyToOne
    @JoinColumn(name = "tutor_id")
    private Tutor tutor;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "reviewer_nickname", length = 128)
    private String nickname;

    @Column(name = "review_text", length = 1024)
    private String text;

    @Column(name = "review_score")
    private Short score;

    @Column(name = "edu_year_start")
    private Short eduStart;

    @Column(name = "edu_year_end")
    private Short eduEnd;

    @Column(name = "review_date")
    private LocalDate reviewDate;

    public Review() {
    }

    public Review(Integer reviewId, Tutor tutor, Course course, String nickname, String text, Short score, Short eduStart, Short eduEnd, LocalDate reviewDate) {
        this.reviewId = reviewId;
        this.tutor = tutor;
        this.course = course;
        this.nickname = nickname;
        this.text = text;
        this.score = score;
        this.eduStart = eduStart;
        this.eduEnd = eduEnd;
        this.reviewDate = reviewDate;
    }

    public Review(Tutor tutor, Course course, String nickname, String text, Short score, Short eduStart, Short eduEnd, LocalDate reviewDate) {
        this.tutor = tutor;
        this.course = course;
        this.nickname = nickname;
        this.text = text;
        this.score = score;
        this.eduStart = eduStart;
        this.eduEnd = eduEnd;
        this.reviewDate = reviewDate;
    }

    @Override
    public String toString() {
        return "ReviewEntity{" +
                "reviewId=" + reviewId +
                ", tutor=" + tutor +
                ", course=" + course +
                ", nickname='" + nickname + '\'' +
                ", text='" + text + '\'' +
                ", score=" + score +
                ", eduStart=" + eduStart +
                ", eduEnd=" + eduEnd +
                ", reviewDate=" + reviewDate +
                '}';
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Short getScore() {
        return score;
    }

    public void setScore(Short score) {
        this.score = score;
    }

    public Short getEduStart() {
        return eduStart;
    }

    public void setEduStart(Short eduStart) {
        this.eduStart = eduStart;
    }

    public Short getEduEnd() {
        return eduEnd;
    }

    public void setEduEnd(Short eduEnd) {
        this.eduEnd = eduEnd;
    }

    public LocalDate getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(LocalDate reviewDate) {
        this.reviewDate = reviewDate;
    }
}
