package ru.nsu.baldezh.service.db_service.service.course;

import ru.nsu.baldezh.service.db_service.model.entity.Course;

public interface ICourseService {
    void addCourse(Course course);
    Iterable<Course> getCourses();
    void deleteCourseById(Integer id);
    void updateCourse(Course course);
}
