package ru.nsu.baldezh.service.db_service.service.tutor;

import ru.nsu.baldezh.service.db_service.model.entity.Tutor;

public interface ITutorService {
    void addTutor(Tutor tutor);
    Iterable<Tutor> getTutors();
    void deleteTutorById(Integer id);
    void updateTutor(Tutor tutor);
}
