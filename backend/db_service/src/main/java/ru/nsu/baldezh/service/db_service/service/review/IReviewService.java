package ru.nsu.baldezh.service.db_service.service.review;

import ru.nsu.baldezh.service.db_service.model.entity.Review;

import java.time.LocalDate;

public interface IReviewService {
    void addProposedReview(Review review);

    void moveReviewToAccepted(Review review);

    void deleteProposedReview(Integer id);

    Iterable<Review> getAllAcceptedReview();

    Iterable<Review> getAllProposedReview();

    Iterable<Review> getFilteredAcceptedReviews(Integer tutorId,
                                                Integer courseId,
                                                Integer score,
                                                LocalDate reviewDateStart,
                                                LocalDate reviewDateEnd,
                                                String orderBy);
}
