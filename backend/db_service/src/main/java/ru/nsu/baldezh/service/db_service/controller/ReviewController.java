package ru.nsu.baldezh.service.db_service.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.baldezh.service.db_service.model.ModelMapper;
import ru.nsu.baldezh.service.db_service.model.dto.Review;
import ru.nsu.baldezh.service.db_service.service.review.IReviewService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path = "/review")
public class ReviewController {
    private final IReviewService reviewService;
    private final ModelMapper modelMapper;

    public ReviewController(IReviewService reviewService, ModelMapper modelMapper) {
        this.reviewService = reviewService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("")
    public ResponseEntity<String> addProposedReview(@RequestBody Review review) {
        reviewService.addProposedReview(modelMapper.map(review));
        return ResponseEntity.ok("Review added");
    }

    @PostMapping("/accept")
    public ResponseEntity<String> acceptProposedReview(@RequestBody Review review) {
        reviewService.moveReviewToAccepted(modelMapper.map(review));
        return ResponseEntity.ok("Review accepted");
    }

    @GetMapping("")
    public ResponseEntity<List<Review>> getAcceptedReviews() {
        var reviews = StreamSupport.stream(reviewService.getAllAcceptedReview().spliterator(), false).map(modelMapper::map).collect(Collectors.toList());
        return ResponseEntity.ok(reviews);
    }

    @GetMapping("/filter")
    public ResponseEntity<List<Review>> getAcceptedReviewsFiltered(
            @RequestParam(value = "tutor_id", required = false) Integer tutorId,
            @RequestParam(value = "course_id", required = false) Integer courseId,
            @RequestParam(value = "score", required = false) Integer score,
            @RequestParam(value = "review_date_start", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate reviewDateStart,
            @RequestParam(value = "review_date_end", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate reviewDateEnd,
            @RequestParam(value = "order_by", required = false) String orderBy
    ) {

        var reviews = StreamSupport.stream(reviewService.getFilteredAcceptedReviews(
                        tutorId,
                        courseId,
                        score,
                        reviewDateStart,
                        reviewDateEnd,
                        orderBy
                ).spliterator(), false)
                .map(modelMapper::map)
                .collect(Collectors.toList());

        return ResponseEntity.ok(reviews);
    }

    @DeleteMapping("")
    public ResponseEntity<String> deleteProposedReview(@RequestParam(value = "id") Integer id) {
        reviewService.deleteProposedReview(id);
        return ResponseEntity.ok("Review deleted");
    }

    @GetMapping("/proposed")
    public ResponseEntity<List<Review>> getProposedReviews() {
        var reviews = StreamSupport.stream(reviewService.getAllProposedReview().spliterator(), false).map(modelMapper::map).collect(Collectors.toList());
        return ResponseEntity.ok(reviews);
    }
}
