package ru.nsu.baldezh.service.db_service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.baldezh.service.db_service.model.entity.ProposedReview;
import ru.nsu.baldezh.service.db_service.model.entity.Review;

import java.util.Optional;

public interface ProposedReviewCrudRepository extends CrudRepository<ProposedReview, Integer> {
    Optional<ProposedReview> findByReview(Review review);
    void deleteProposedReviewByReviewReviewId(Integer id);
}
