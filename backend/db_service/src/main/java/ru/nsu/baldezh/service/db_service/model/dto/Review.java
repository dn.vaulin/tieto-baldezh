package ru.nsu.baldezh.service.db_service.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class Review {
    @JsonProperty("reviewId") private Integer reviewId;
    @JsonProperty("tutor") private Tutor tutor;
    @JsonProperty("course") private Course course;
    @JsonProperty("nickname") private String nickname;
    @JsonProperty("text") private String text;
    @JsonProperty("score") private Short score;
    @JsonProperty("eduStart") private Short eduStart;
    @JsonProperty("eduEnd") private Short eduEnd;
    @JsonProperty("reviewDate") private LocalDate reviewDate;

    public Review() {
    }

    public Review(Integer reviewId, Tutor tutor, Course course, String nickname, String text, Short score, Short eduStart, Short eduEnd, LocalDate reviewDate) {
        this.reviewId = reviewId;
        this.tutor = tutor;
        this.course = course;
        this.nickname = nickname;
        this.text = text;
        this.score = score;
        this.eduStart = eduStart;
        this.eduEnd = eduEnd;
        this.reviewDate = reviewDate;
    }

    public Review(Tutor tutor, Course course, String nickname, String text, Short score, Short eduStart, Short eduEnd, LocalDate reviewDate) {
        this.tutor = tutor;
        this.course = course;
        this.nickname = nickname;
        this.text = text;
        this.score = score;
        this.eduStart = eduStart;
        this.eduEnd = eduEnd;
        this.reviewDate = reviewDate;
    }

    @Override
    public String toString() {
        return "ReviewDTO{" +
                "reviewId=" + reviewId +
                ", tutor=" + tutor +
                ", course=" + course +
                ", nickname='" + nickname + '\'' +
                ", text='" + text + '\'' +
                ", score=" + score +
                ", eduStart=" + eduStart +
                ", eduEnd=" + eduEnd +
                ", reviewDate=" + reviewDate +
                '}';
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Short getScore() {
        return score;
    }

    public void setScore(Short score) {
        this.score = score;
    }

    public Short getEduStart() {
        return eduStart;
    }

    public void setEduStart(Short eduStart) {
        this.eduStart = eduStart;
    }

    public Short getEduEnd() {
        return eduEnd;
    }

    public void setEduEnd(Short eduEnd) {
        this.eduEnd = eduEnd;
    }

    public LocalDate getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(LocalDate reviewDate) {
        this.reviewDate = reviewDate;
    }
}
