package ru.nsu.baldezh.service.db_service.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.baldezh.service.db_service.model.ModelMapper;
import ru.nsu.baldezh.service.db_service.model.dto.Course;
import ru.nsu.baldezh.service.db_service.model.dto.Tutor;
import ru.nsu.baldezh.service.db_service.model.dto.TutorCourse;
import ru.nsu.baldezh.service.db_service.service.tutorcourses.ITutorCourseService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/tutcour")
public class TutorCourseController {
    private final ITutorCourseService tutorCourseService;
    private final ModelMapper modelMapper;

    public TutorCourseController(ITutorCourseService tutorCourseService, ModelMapper modelMapper) {
        this.tutorCourseService = tutorCourseService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("")
    public ResponseEntity<List<TutorCourse>> getAll() {
        var tutorsCourses = StreamSupport.stream(tutorCourseService.getAll().spliterator(), false).map(modelMapper::map).collect(Collectors.toList());
        return ResponseEntity.ok(tutorsCourses);
    }

    @PostMapping("")
    public ResponseEntity<String> addTutorCourse(@RequestBody TutorCourse tutorCourse) {
        tutorCourseService.addTutorCourse(modelMapper.map(tutorCourse));
        return ResponseEntity.ok("TutorCourse added");
    }

    @GetMapping("/courses")
    public ResponseEntity<List<Course>> findCoursesByTutor(@RequestParam(value = "tutor_id") Integer tutorId) {
        var courses = StreamSupport
                .stream(tutorCourseService.findCoursesByTutor(tutorId).spliterator(), false)
                .map(modelMapper::map)
                .collect(Collectors.toList());

        return ResponseEntity.ok(courses);
    }

    @GetMapping("/tutors")
    public ResponseEntity<List<Tutor>> findTutorsByCourse(@RequestParam(value = "course_id") Integer courseId) {
        var tutors = StreamSupport
                .stream(tutorCourseService.findTutorsByCourse(courseId).spliterator(), false)
                .map(modelMapper::map)
                .collect(Collectors.toList());

        return ResponseEntity.ok(tutors);
    }
}
