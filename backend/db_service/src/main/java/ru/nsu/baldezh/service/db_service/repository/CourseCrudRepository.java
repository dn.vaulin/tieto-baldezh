package ru.nsu.baldezh.service.db_service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.baldezh.service.db_service.model.entity.Course;

public interface CourseCrudRepository extends CrudRepository<Course, Integer> {

}
