package ru.nsu.baldezh.service.db_service.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "proposed_review")
public class ProposedReview {
    @Id
    @Column(name = "record_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer recordId;

    @OneToOne
    @JoinColumn(name = "review_id", referencedColumnName = "review_id")
    private Review review;

    public ProposedReview() {
    }

    public ProposedReview(Review review) {
        this.review = review;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}
