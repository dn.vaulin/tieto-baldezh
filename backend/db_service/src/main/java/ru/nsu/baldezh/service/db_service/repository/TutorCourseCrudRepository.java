package ru.nsu.baldezh.service.db_service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.baldezh.service.db_service.model.entity.TutorCourse;

public interface TutorCourseCrudRepository extends CrudRepository<TutorCourse, Integer> {
    Iterable<TutorCourse> findAllByCourseCourseId(Integer courseId);
    Iterable<TutorCourse> findAllByTutorTutorId(Integer tutorId);
}
