package ru.nsu.baldezh.service.db_service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.baldezh.service.db_service.model.ModelMapper;
import ru.nsu.baldezh.service.db_service.model.dto.Course;
import ru.nsu.baldezh.service.db_service.service.course.ICourseService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller()
@RequestMapping("/course")
public class CourseController {
    private final ICourseService courseService;
    private final ModelMapper modelMapper;

    public CourseController(ICourseService courseService, ModelMapper modelMapper) {
        this.courseService = courseService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(path = "")
    public ResponseEntity<String> addNewTutor(@RequestBody Course course) {
        courseService.addCourse(modelMapper.map(course));
        return ResponseEntity.ok("Course added");
    }

    @GetMapping(path = "")
    public ResponseEntity<List<Course>> getAllCourses() {
        var courses = StreamSupport.stream(courseService.getCourses().spliterator(), false).map(modelMapper::map).collect(Collectors.toList());
        return ResponseEntity.ok(courses);
    }

    @DeleteMapping("")
    public ResponseEntity<String> deleteCourse(@RequestParam(value = "id") Integer id) {
        courseService.deleteCourseById(id);
        return ResponseEntity.ok("Course deleted");
    }

    @PutMapping("")
    public ResponseEntity<?> updateCourse(@RequestBody Course course) {
        courseService.updateCourse(modelMapper.map(course));
        return ResponseEntity.ok("Course updated");
    }
}
