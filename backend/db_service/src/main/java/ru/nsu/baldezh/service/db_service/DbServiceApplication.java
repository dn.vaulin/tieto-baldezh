package ru.nsu.baldezh.service.db_service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbServiceApplication {
	private static final Logger log = LoggerFactory.getLogger(DbServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DbServiceApplication.class, args);
	}
}
