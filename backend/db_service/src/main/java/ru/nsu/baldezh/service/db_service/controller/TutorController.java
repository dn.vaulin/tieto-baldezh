package ru.nsu.baldezh.service.db_service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.baldezh.service.db_service.model.ModelMapper;
import ru.nsu.baldezh.service.db_service.model.dto.Tutor;
import ru.nsu.baldezh.service.db_service.service.tutor.ITutorService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(path = "/tutor")
public class TutorController {
    private final ITutorService tutorService;
    private final ModelMapper modelMapper;

    public TutorController(ITutorService tutorService, ModelMapper modelMapper) {
        this.tutorService = tutorService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(path = "")
    public ResponseEntity<String> addNewTutor(@RequestBody Tutor tutor) {
        tutorService.addTutor(modelMapper.map(tutor));
        return ResponseEntity.ok("Tutor added");
    }

    @GetMapping(path = "")
    public ResponseEntity<List<Tutor>> getAllTutors() {
        var tutors = StreamSupport.stream(tutorService.getTutors().spliterator(), false).map(modelMapper::map).collect(Collectors.toList());
        return ResponseEntity.ok(tutors);
    }

    @DeleteMapping(path = "")
    public ResponseEntity<String> deleteTutorById(@RequestParam(value = "id") Integer id) {
        tutorService.deleteTutorById(id);
        return ResponseEntity.ok("Tutor updated");
    }

    @PutMapping(path = "")
    public ResponseEntity<String> updateTutor(@RequestBody Tutor tutor) {
        tutorService.updateTutor(modelMapper.map(tutor));
        return ResponseEntity.ok("Tutor updated");
    }

}
