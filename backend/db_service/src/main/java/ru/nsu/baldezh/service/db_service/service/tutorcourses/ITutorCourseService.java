package ru.nsu.baldezh.service.db_service.service.tutorcourses;

import ru.nsu.baldezh.service.db_service.model.entity.Course;
import ru.nsu.baldezh.service.db_service.model.entity.Tutor;
import ru.nsu.baldezh.service.db_service.model.entity.TutorCourse;

public interface ITutorCourseService {
    Iterable<TutorCourse> getAll();
    Iterable<Course> findCoursesByTutor(Integer tutorId);
    Iterable<Tutor> findTutorsByCourse(Integer courseId);
    void addTutorCourse(TutorCourse tutorCourse);
}
