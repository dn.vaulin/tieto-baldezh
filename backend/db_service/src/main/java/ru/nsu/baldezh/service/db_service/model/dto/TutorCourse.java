package ru.nsu.baldezh.service.db_service.model.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

public class TutorCourse {
    @JsonProperty("recordId") private Integer recordId;
    @JsonProperty("tutor") private Tutor tutor;
    @JsonProperty("course") private Course course;

    public TutorCourse() {
    }

    public TutorCourse(Integer recordId, Tutor tutor, Course course) {
        this.recordId = recordId;
        this.tutor = tutor;
        this.course = course;
    }

    public TutorCourse(Tutor tutor, Course course) {
        this.tutor = tutor;
        this.course = course;
    }

    @Override
    public String toString() {
        return "TutorCourseDTO{" +
                "recordId=" + recordId +
                ", tutor=" + tutor +
                ", course=" + course +
                '}';
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
