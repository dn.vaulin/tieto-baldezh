package ru.nsu.baldezh.service.db_service.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "tutors_courses")
public class TutorCourse {
    @Id
    @Column(name = "record_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer recordId;

    @ManyToOne
    @JoinColumn(name = "tutor_id")
    private Tutor tutor;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    public TutorCourse() {
    }

    public TutorCourse(Integer recordId, Tutor tutor, Course course) {
        this.recordId = recordId;
        this.tutor = tutor;
        this.course = course;
    }

    public TutorCourse(Tutor tutor, Course course) {
        this.tutor = tutor;
        this.course = course;
    }

    @Override
    public String toString() {
        return "TutorCourseEntity{" +
                "recordId=" + recordId +
                ", tutor=" + tutor +
                ", course=" + course +
                '}';
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
