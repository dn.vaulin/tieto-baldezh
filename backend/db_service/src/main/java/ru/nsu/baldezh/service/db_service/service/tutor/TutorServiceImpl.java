package ru.nsu.baldezh.service.db_service.service.tutor;

import org.springframework.stereotype.Service;
import ru.nsu.baldezh.service.db_service.model.entity.Tutor;
import ru.nsu.baldezh.service.db_service.repository.TutorCrudRepository;

@Service
public class TutorServiceImpl implements ITutorService {
    private final TutorCrudRepository tutorCrudRepository;

    public TutorServiceImpl(TutorCrudRepository tutorCrudRepository) {
        this.tutorCrudRepository = tutorCrudRepository;
    }

    @Override
    public void addTutor(Tutor tutor) {
        tutorCrudRepository.save(tutor);
    }

    @Override
    public Iterable<Tutor> getTutors() {
        return tutorCrudRepository.findAll();
    }

    @Override
    public void deleteTutorById(Integer id) {
        tutorCrudRepository.deleteById(id);
    }

    @Override
    public void updateTutor(Tutor tutor) {
        tutorCrudRepository.save(tutor);
    }
}
