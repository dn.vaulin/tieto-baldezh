package ru.nsu.baldezh.service.db_service.service.course;

import org.springframework.stereotype.Service;
import ru.nsu.baldezh.service.db_service.model.entity.Course;
import ru.nsu.baldezh.service.db_service.repository.CourseCrudRepository;

@Service
public class CourseServiceImpl implements ICourseService {
    private final CourseCrudRepository courseCrudRepository;

    public CourseServiceImpl(CourseCrudRepository courseCrudRepository) {
        this.courseCrudRepository = courseCrudRepository;
    }

    @Override
    public void addCourse(Course course) {
        courseCrudRepository.save(course);
    }

    @Override
    public Iterable<Course> getCourses() {
        return courseCrudRepository.findAll();
    }

    @Override
    public void deleteCourseById(Integer id) {
        courseCrudRepository.deleteById(id);
    }

    @Override
    public void updateCourse(Course course) {
        courseCrudRepository.save(course);
    }
}
