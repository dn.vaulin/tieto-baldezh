package ru.nsu.baldezh.service.db_service.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.nsu.baldezh.service.db_service.model.entity.AcceptedReview;

import java.time.LocalDate;

public interface AcceptedReviewCrudRepository extends CrudRepository<AcceptedReview, Integer> {

    @Query("SELECT r " +
            "FROM AcceptedReview r " +
            "WHERE (:tutorId IS NULL OR r.review.tutor.tutorId = :tutorId) " +
            "AND (:courseId IS NULL OR r.review.course.courseId = :courseId) " +
            "AND (:score IS NULL OR r.review.score = :score) " +
            "AND (r.review.reviewDate between :reviewDateStart and :reviewDateEnd)"
    )
    Iterable<AcceptedReview> findFilteredAcceptedReviews(
            @Param("tutorId") Integer tutorId,
            @Param("courseId") Integer courseId,
            @Param("score") Integer score,
            @Param("reviewDateStart") LocalDate reviewDateStart,
            @Param("reviewDateEnd") LocalDate reviewDateEnd,
            Sort sort
    );



    /*@Query(value = "SELECT ar " +
            "FROM accepted_review ar inner join review r on cast(r.review_id as integer) = ar.review_id " +
            "WHERE (cast(r.tutor_Id as integer) = :tutorId OR :tutorId IS NULL) " +
            "AND (cast(r.course_Id as integer) = :courseId or :courseId IS NULL) " +
            "AND (cast(r.score as integer) = cast(:score as integer) or :score IS NULL) " +
            "AND (cast(r.review_date as date) >= cast(:reviewDateStart as date) or :reviewDateStart IS NULL) " +
            "AND (cast(r.review_date as date) <= cast(:reviewDateEnd as date) or :reviewDateEnd IS NULL) ",
            nativeQuery = true
    )
    Collection<AcceptedReview> findFilteredAcceptedReviews(
            @Param("tutorId") Integer tutorId,
            @Param("courseId") Integer courseId,
            @Param("score") Integer score,
            @Param("reviewDateStart") Date reviewDateStart,
            @Param("reviewDateEnd") Date reviewDateEnd
    );*/
}
