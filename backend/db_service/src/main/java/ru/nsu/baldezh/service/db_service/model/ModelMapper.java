package ru.nsu.baldezh.service.db_service.model;

import org.springframework.stereotype.Service;
import ru.nsu.baldezh.service.db_service.model.dto.Course;
import ru.nsu.baldezh.service.db_service.model.dto.Review;
import ru.nsu.baldezh.service.db_service.model.dto.Tutor;
import ru.nsu.baldezh.service.db_service.model.dto.TutorCourse;

@Service
public class ModelMapper {
    public ru.nsu.baldezh.service.db_service.model.entity.Tutor map(Tutor tutor) {
        return tutor == null ? null : new ru.nsu.baldezh.service.db_service.model.entity.Tutor(tutor.getTutorId(), tutor.getFirstName(), tutor.getLastName(), tutor.getPatronymic());
    }

    public Tutor map(ru.nsu.baldezh.service.db_service.model.entity.Tutor tutor) {
        return tutor == null ? null : new Tutor(tutor.getTutorId(), tutor.getFirstName(), tutor.getLastName(), tutor.getPatronymic());
    }

    public ru.nsu.baldezh.service.db_service.model.entity.Course map(Course course) {
        return course == null ? null : new ru.nsu.baldezh.service.db_service.model.entity.Course(course.getCourseId(), course.getCourseName());
    }

    public Course map(ru.nsu.baldezh.service.db_service.model.entity.Course course) {
        return course == null ? null : new Course(course.getCourseId(), course.getCourseName());
    }

    public ru.nsu.baldezh.service.db_service.model.entity.Review map(Review review) {
        return review == null ? null : new ru.nsu.baldezh.service.db_service.model.entity.Review(
                review.getReviewId(),
                map(review.getTutor()),
                map(review.getCourse()),
                review.getNickname(),
                review.getText(),
                review.getScore(),
                review.getEduStart(),
                review.getEduEnd(),
                review.getReviewDate()
        );
    }

    public Review map(ru.nsu.baldezh.service.db_service.model.entity.Review review) {
        return review == null ? null : new Review(
                review.getReviewId(),
                map(review.getTutor()),
                map(review.getCourse()),
                review.getNickname(),
                review.getText(),
                review.getScore(),
                review.getEduStart(),
                review.getEduEnd(),
                review.getReviewDate()
        );
    }

    public ru.nsu.baldezh.service.db_service.model.entity.TutorCourse map(TutorCourse tutorCourse) {
        return tutorCourse == null ? null : new ru.nsu.baldezh.service.db_service.model.entity.TutorCourse(tutorCourse.getRecordId(), map(tutorCourse.getTutor()), map(tutorCourse.getCourse()));
    }

    public TutorCourse map(ru.nsu.baldezh.service.db_service.model.entity.TutorCourse tutorCourse) {
        return tutorCourse == null ? null : new TutorCourse(tutorCourse.getRecordId(), map(tutorCourse.getTutor()), map(tutorCourse.getCourse()));
    }
}
