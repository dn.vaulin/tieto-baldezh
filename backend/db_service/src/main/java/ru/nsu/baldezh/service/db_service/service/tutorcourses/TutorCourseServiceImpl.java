package ru.nsu.baldezh.service.db_service.service.tutorcourses;

import org.springframework.stereotype.Service;
import ru.nsu.baldezh.service.db_service.model.entity.Course;
import ru.nsu.baldezh.service.db_service.model.entity.Tutor;
import ru.nsu.baldezh.service.db_service.model.entity.TutorCourse;
import ru.nsu.baldezh.service.db_service.repository.TutorCourseCrudRepository;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TutorCourseServiceImpl implements ITutorCourseService {
    private final TutorCourseCrudRepository tutorCourseCrudRepository;

    public TutorCourseServiceImpl(TutorCourseCrudRepository tutorCourseCrudRepository) {
        this.tutorCourseCrudRepository = tutorCourseCrudRepository;
    }

    @Override
    public Iterable<TutorCourse> getAll() {
        return tutorCourseCrudRepository.findAll();
    }

    @Override
    public Iterable<Course> findCoursesByTutor(Integer tutorId) {
        return StreamSupport
                .stream(tutorCourseCrudRepository.findAllByTutorTutorId(tutorId).spliterator(), false)
                .map(TutorCourse::getCourse)
                .collect(Collectors.toList());
    }

    @Override
    public Iterable<Tutor> findTutorsByCourse(Integer courseId) {
        return StreamSupport
                .stream(tutorCourseCrudRepository.findAllByCourseCourseId(courseId).spliterator(), false)
                .map(TutorCourse::getTutor)
                .collect(Collectors.toList());
    }

    @Override
    public void addTutorCourse(TutorCourse tutorCourse) {
        tutorCourseCrudRepository.save(tutorCourse);
    }
}
