package ru.nsu.baldezh.service.db_service.service.review;

import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.baldezh.service.db_service.model.entity.AcceptedReview;
import ru.nsu.baldezh.service.db_service.model.entity.ProposedReview;
import ru.nsu.baldezh.service.db_service.model.entity.Review;
import ru.nsu.baldezh.service.db_service.repository.AcceptedReviewCrudRepository;
import ru.nsu.baldezh.service.db_service.repository.ProposedReviewCrudRepository;
import ru.nsu.baldezh.service.db_service.repository.ReviewCrudRepository;

import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ReviewServiceImpl implements IReviewService {
    private final ReviewCrudRepository reviewCrudRepository;
    private final ProposedReviewCrudRepository proposedReviewCrudRepository;
    private final AcceptedReviewCrudRepository acceptedReviewCrudRepository;

    public ReviewServiceImpl(ReviewCrudRepository reviewCrudRepository, ProposedReviewCrudRepository proposedReviewCrudRepository, AcceptedReviewCrudRepository acceptedReviewCrudRepository) {
        this.reviewCrudRepository = reviewCrudRepository;
        this.proposedReviewCrudRepository = proposedReviewCrudRepository;
        this.acceptedReviewCrudRepository = acceptedReviewCrudRepository;
    }

    @Transactional
    @Override
    public void deleteProposedReview(Integer id) {
        proposedReviewCrudRepository.deleteProposedReviewByReviewReviewId(id);
        reviewCrudRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void addProposedReview(Review review) {
        review = reviewCrudRepository.save(review);
        proposedReviewCrudRepository.save(new ProposedReview(review));
    }

    @Transactional
    @Override
    public void moveReviewToAccepted(Review review) {
        proposedReviewCrudRepository.findByReview(review).ifPresent(proposedReview -> {
            acceptedReviewCrudRepository.save(new AcceptedReview(review));
            proposedReviewCrudRepository.delete(proposedReview);
        });
    }

    @Override
    public Iterable<Review> getAllAcceptedReview() {
        return StreamSupport.stream(acceptedReviewCrudRepository.findAll().spliterator(), false).map(AcceptedReview::getReview).collect(Collectors.toList());
    }

    @Override
    public Iterable<Review> getAllProposedReview() {
        return StreamSupport.stream(proposedReviewCrudRepository.findAll().spliterator(), false).map(ProposedReview::getReview).collect(Collectors.toList());
    }

    @Override
    public Iterable<Review> getFilteredAcceptedReviews(Integer tutorId, Integer courseId, Integer score, LocalDate reviewDateStart, LocalDate reviewDateEnd, String orderBy) {

        if (orderBy == null) {
            orderBy = "review.reviewDate";
        } else {
            orderBy = "review." + orderBy;
        }

        if (reviewDateStart == null) {
            reviewDateStart = LocalDate.of(2000, 10, 10);
        }

        if (reviewDateEnd == null) {
            reviewDateEnd = LocalDate.now().plusDays(1);
        }

        return StreamSupport.stream(acceptedReviewCrudRepository.findFilteredAcceptedReviews(
                tutorId,
                courseId,
                score,
                reviewDateStart,
                reviewDateEnd,
                JpaSort.unsafe(orderBy)
        ).spliterator(), false).map(AcceptedReview::getReview).collect(Collectors.toList());
    }
}
