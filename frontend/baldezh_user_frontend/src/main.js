import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { VueRecaptcha } from "vue-recaptcha";

const app = createApp(App);
app.use(router).mount("#app");
app.component("vue-recaptcha", VueRecaptcha)