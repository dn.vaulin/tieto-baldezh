import { createRouter, createWebHistory } from "vue-router";
import ReviewList from "../views/ReviewList.vue";
import Feedback from "../views/Feedback";

const routes = [
  {
    path: "/",
    name: "ReviewList",
    component: ReviewList,
  },
  {
    path: "/feedback",
    name: "Feedback",
    component: Feedback,
  },
  {
    path: "/create_review",
    name: "CreateReview",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CreateReview.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
