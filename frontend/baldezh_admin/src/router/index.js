import { createRouter, createWebHistory } from "vue-router";
import Course from "../views/Course";
import Tutor from "../views/Tutor";
import Review from "../views/Review";
import Auth from "../views/Auth";

const routes = [
  {
    path: "/course",
    name: "Course",
    component: Course,
  },
  {
    path: "/tutor",
    name: "Tutor",
    component: Tutor,
  },
  {
    path: "/review",
    name: "Review",
    component: Review,
  },
  {
    path: "/auth",
    name: "Auth",
    component: Auth,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
